#!/bin/bash

export LD_LIBRARY_PATH="."

if [ "$#" -ne 2 ]; then
  echo "
  Invalid number of arguments!
  
  --USAGE--
  lpscheduler.sh <input_file> <output_file>
  
  This program requires two arguments. The first is the input
  file which contains the jobs and processes. The second argument
  is a file to output the results to. If this output file does not
  exist, then it will be created; otherwise, the existing file will
  be overwritten.
  
  Example: ./lpscheduler.sh input.txt output.txt
  "
else
  java -jar lpscheduler.jar $1 $2
fi 
