Name      : Shawn Nikkila
Email     : scnikkila@gmail.com
Date      : 10 October 2013

INTRODUCTION
-------------------------------------------------------------------------
This is a program that optimally solves the scheduling problem using a
linear programming approach. It was implemented for a class project and uses
the approach outlined in the following research paper:

"On a graph partition problem with application to VLSI layout" by
Arunabha Sen, Haiyong Deng, Sumanta Guha

This program utilizes the JGraphT library (http://jgrapht.org/) for graph 
construction and for finding maximal cliques. It also uses the lpsolve libraries
(http://sourceforge.net/projects/lpsolve/) for constructing and solving the LP 
problem instance.

EXECUTING THE APPLICATION
-------------------------------------------------------------------------
UNIX (64 bit):
It is recommended to run the program using the shell script provided
in the bin directory. The shell script is named "lpscheduler.sh". Make 
sure that the script has the proper permissions before executing it. 
The bin directory contains two shared object (SO) libraries and a JAR
file (lpscheduler.jar) that MUST be in the same directory as the 
lpscheduler.sh file. To execute the program, execute the following command 
in the terminal:

/path/to/lpscheduler.sh <input_file> <output_file>

Replace <input_file with the location of the input file and replace 
<output_file> with the location where the output file should be written.

Example:
./lpscheduler.sh input.txt output.txt

The above command will execute the application with the "input.txt" file
found in the same directory as the shell script and create an output file
named "output.txt" in the same directory as the shell script. The output
will also be displayed in the terminal.

WINDOWS (64 bit):
It is recommended to run the program using the JAR file provided in
the bin directory. The bin directory contains two DLL files that must
be in the same directory as the lpscheduler.jar file. to execute the
program, execute the following command in the command line:

java -jar lpscheduler.jar <input_file> <output_file>

Replace <input_file> with the location of the input file and replace
<output_file> with the location where the output file should be written.

Example:
java -jar lpscheduler.jar input.txt output.txt

The above command will execute the application with the "input.txt" file
found in the same directory as the JAR file and create an output file
named "output.txt" in the same directory as the JAR file. The output
will also be displayed in the console.