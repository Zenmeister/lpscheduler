package org.nikkila.cse555;

/**
 * This is a container class for holding each job. Simply
 * stores the start and finish times for the job.
 * 
 * @author Shawn Nikkila
 * @assignment CSE550 HW1
 */
public class Job 
{
	protected double _start;
	protected double _finish;
	
	public static boolean isOverlapping(Job j1, Job j2)
	{
		boolean result = false;
		
		double j1s = j1.getStart();
		double j1f = j1.getFinish();
		
		double j2s = j2.getStart();
		double j2f = j2.getFinish();
		
		// Checks the following:
		// 1) Start of j1 overlaps with j2.
		// 2) End of j1 overlaps with j2.
		if ((j1s >= j2s && j1s < j2f) || (j1s <= j2s && j1f > j2s))
		{
			result = true;
		}
		
		return result;
	}
	
	public Job(double start, double finish)
	{
		_start = start;
		_finish = finish;
	}
	
	public double getStart()
	{
		return _start;
	}
	
	public void setStart(double value)
	{
		_start = value;
	}
	
	public double getFinish()
	{
		return _finish;
	}
	
	public void setFinish(double value)
	{
		_finish = value;
	}
	
	public String toString()
	{
		return "Start: " + _start + "; Finish: " + _finish;
	}
}
