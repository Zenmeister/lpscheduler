
package org.nikkila.cse555;

/**
 * Main driver class.
 * 
 * @author Shawn Nikkila
 * @assignment CSE550 HW1
 */
public class LPScheduler {

	/**
	 * Main method.
	 * 
	 * @param args Takes in two arguments. The first is
	 * the input file and the second is the output file.
	 */
	public static void main(String[] args) 
	{	
		Scheduler s = new Scheduler();
		
		if (args.length == 2)
			s.partition(args[0], args[1]);
		else
		{
			System.out.println();
			System.out.println("-------------------------------------");
			System.out.println("USAGE");
			System.out.println("-------------------------------------");
			System.out.println();
			System.out.println("java -jar assignment1.jar <input_file> <output_file>");
			System.out.println();
			System.out.println(
					"This program requires two arguments. The first is " + 
					"the input file which contains the jobs and processes. " +
					"The second argument is a file to output the results to. " +
					"If this output file does not exist, then it will be " +
					"created; otherwise the existing file will be overwritten. ");
		}
	}
}
