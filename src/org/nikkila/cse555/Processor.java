package org.nikkila.cse555;

/**
 * A simple container class for the processors. Simply
 * holds the processing cost for the processor.
 * 
 * @author Shawn Nikkila
 * @assignment CSE550 HW1
 */
public class Processor 
{
	protected double _cost;
	
	public Processor(double cost)
	{
		_cost = cost;
	}
	
	public double getCost()
	{
		return _cost;
	}
	
	public void setCost(double value)
	{
		_cost = value;
	}
	
	public String toString()
	{
		return "Cost: " + _cost;
	}
}
