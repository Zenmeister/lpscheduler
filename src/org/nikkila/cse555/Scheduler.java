package org.nikkila.cse555;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 * This class is responsible for partitioning a number of jobs
 * into processors so that the job schedule are not overlapping.
 * 
 * @author Shawn Nikkila
 * @assignment CSE550 HW1
 */
public class Scheduler 
{
	public static boolean DEBUG = false;
	
	// For convenience sake, a variable to store the number of
	// jobs.
	private int N;
	
	// Stores the jobs and processor costs.
	protected Job [] _jobs;
	protected Processor [] _processors;
	
	// Stores the graph
	protected UndirectedGraph<Integer, DefaultEdge> _graph;
	
	
	/**
	 * Empty default constructor.
	 */
	public Scheduler() { }
	
	/**
	 * Assigns all jobs to a processor so that the processing cost
	 * is minimized. Accomplishes this by utilizing a linear
	 * programming approach.
	 * 
	 * @param input_file The file containing the jobs and processes 
	 * for this problem instance.
	 * 
	 * @param output_file An output file which contains the results
	 * of the partitioning.
	 */
	public void partition(String input_file, String output_file)
	{
		// Read the file and store the jobs and processor costs
		// into the _jobs and _processors array.
		readFile(input_file);
		
		// Constructs the graph based on the input read from the file.
		constructGraph();
		
		// Store the partitions in here.
		ArrayList<ArrayList<Integer>> partitions = 
				new ArrayList<ArrayList<Integer>>();
		
		// Construct and solve the linear programming problem. This
		// does a standard, polynomial time LP solve.
		double [] lp_results = solveLpProblem(partitions, false);
		
		// If the second element of the results array is -1.0, then
		// no integral solution could be found. Therefore, solve the
		// same problem, except with ILP.
		if (lp_results[1] == -1.0)
		{
			System.err.println("Integral solution not found. Solving with ILP...");
			lp_results = solveLpProblem(partitions, true);
		}
		
		// Output the results to a file.
		writeResults(partitions, lp_results[0], output_file);
	}
	
	
	/**
	 * Helper method which reads in the input file and extracts the
	 * process costs and jobs from it.
	 * 
	 * @param input_file The file containing the jobs and processes
	 * for this problem instance.
	 */
	private void readFile(String input_file)
	{
		try
		{
			BufferedReader input = new BufferedReader(new FileReader(input_file));
			
			// Get number of jobs
			N = Integer.parseInt(input.readLine());
			
			String [] str_costs = input.readLine().split(" ");
			
			// Instantiate the arrays which will store the processor
			// costs and the number of jobs.
			_processors = new Processor[str_costs.length];
			_jobs       = new Job[N];
			
			// Convert the processor costs to double values and store
			// them into the array.
			for (int i = 0; i < str_costs.length; i++)
			{
				double cost = Double.parseDouble(str_costs[i]);
				_processors[i] = new Processor(cost);
			}
			
			// Read the jobs from the file, convert the start and end times
			// to double values and store them in the array.
			for (int i = 0; i < N; i++)
			{
				String [] job = input.readLine().split(" ");
				
				double start = Double.parseDouble(job[0]);
				double end   = Double.parseDouble(job[1]);
				
				_jobs[i] = new Job(start, end);
			}
			
			input.close();
			
			// ----------------------------------------------------
			// DEBUG CODE
			// ----------------------------------------------------
			if (DEBUG)
			{
				System.out.println("Number of Jobs: " + N);
				
				for (int i = 0; i < _processors.length; i++)
					System.out.println("P" + i + ": " + _processors[i]);
				
				for (int i = 0; i < _jobs.length; i++)
					System.out.println("J" + i + ": " + _jobs[i]);
			}
			// ----------------------------------------------------
		}
		catch (IOException ex)
		{
			System.err.println("Error reader file...");
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * Helper function which writes the results to a file.
	 * 
	 * @param results The partitioning results.
	 * @param obj_cost The value of calculating the objective function.
	 * @param output_file The file to output to.
	 */
	private void writeResults(
			ArrayList<ArrayList<Integer>> results, 
			double obj_cost, 
			String output_file)
	{
		PrintWriter writer = null;
		
		try
		{
			writer = new PrintWriter(output_file);
			
			System.out.println("\nThe total processing cost is = " + obj_cost);
			writer.println("\nThe total processing cost is = " + obj_cost);
			
			for (int i = 0; i < results.size(); i++)
			{
				ArrayList<Integer> jobs = results.get(i);
				
				String str_job = "";
				for (int j = 0; j < jobs.size(); j++)
				{
					str_job += (jobs.get(j) + 1) + " ";
					
				}
				
				System.out.println("Processor P" + (i + 1) + 
						" is assigned the following jobs = {" + str_job.trim() + "}");
				
				writer.println("Processor P" + (i + 1) + 
						" is assigned the following jobs = {" + str_job.trim() + "}");
			}
			
			System.out.println("\n\n-----------------------------------");
			System.out.println("Output file created at '" + output_file + "'");
			System.out.println("-----------------------------------");
		}
		catch (FileNotFoundException ex)
		{
			System.err.println("Output file location not found...");
			ex.printStackTrace();
		}
		finally
		{
			if (writer != null)
				writer.close();
		}
	}
	
	
	/**
	 * Helper method which constructs the graph based on the input. This
	 * must be called after the input file has been read.
	 */
	private void constructGraph()
	{
		// Constructing a simple, undirected graph with non-weighted
		// edges.
		_graph = new SimpleGraph<Integer, DefaultEdge>(DefaultEdge.class);
		
		// Add vertices to the graph. Could combine this with
		// the edge construction, but this keeps it simpler and
		// easier to read.
		for (int i = 0; i < _jobs.length; i++)
			_graph.addVertex(i);
	
		// Checks every vertex pair and constructs an edge between
		// them if necessary.
		for (int i = 0; i < _jobs.length; i++)
		{
			for (int j = i + 1; j < _jobs.length; j++)
			{
				Job j1 = _jobs[i];
				Job j2 = _jobs[j];
				
				// Check if the time intervals overlap between the
				// two processes. If there is an overlap, then add
				// an edge between the vertices.
				if (Job.isOverlapping(j1, j2))
				{
					if (DEBUG)
					{
						System.out.println("Edge connected between " + i + " and " + j);
						System.out.println(j1);
						System.out.println(j2);
						System.out.println();
					}
					
					_graph.addEdge(i, j);
				}
			}
		}
	}
	
	
	/**
	 * Helper method which solves the linear programming problem instance.
	 * Must be called after the problem instance has been read and
	 * the graph has been constructed. Writes the solution to the output file
	 * specified.
	 * 
	 * @param An output variable that stores the job partitions.
	 * 
	 * @return The object function result.
	 */
	private double [] solveLpProblem(ArrayList<ArrayList<Integer>> partitions, boolean use_ilp)
	{
		// Stores the results to return. The first element should be
		// the calculated objective function cost, and the second value
		// is 0.0 if the value is integral and -1.0 if it is not integral.
		double [] results = new double[2];
		
		// Solver
		LpSolve solver = null;
		
		// Get the necessary constraints from the graph.
		ArrayList<String> sc_constraints = straightCliqueConstraints();
		ArrayList<String> cc_constraints = crossCliqueConstraints();
		ArrayList<String> vv_constraints = variableConstraints();
		
		// Gets the objective function.
		String obj_function = constructObjectiveFunction();
		
		try
		{
			// Instantiate the solver. Start with 0 constraints
			// with N x N variables.
			solver = LpSolve.makeLp(0, N * N);
			
			// Add the necessary constraints to solve the problem.
			addLpConstraints(sc_constraints, solver, LpSolve.LE, 1);
			addLpConstraints(cc_constraints, solver, LpSolve.EQ, 1);

			// If we set this method to use ILP, then set all the columns
			// to be binary (0 or 1). This should only be invoked if no
			// satisfactory LP solution can be found which should never
			// happen.
			if (use_ilp)
			{
				for (int i = 0; i < solver.getNorigColumns(); i++)
					solver.setBinary(i + 1, true);
			}
			// Otherwise, add the >= 0 constraints for each of the variables.
			else
				addLpConstraints(vv_constraints, solver, LpSolve.GE, 0);
			
			// Set the objective function.
			solver.strSetObjFn(obj_function);
			
			// Solve the problem.
			solver.solve();
			
			// ----------------------------------------------------
			// DEBUG CODE
			// ----------------------------------------------------
			if (DEBUG)
			{
				solver.printLp();
				solver.printSolution(1);
			}
			// ----------------------------------------------------
			
			// Store the result of the objective function computation.
			results[0] = solver.getObjective();
			
			if (partitions == null)
				partitions = new ArrayList<ArrayList<Integer>>();
			else
				partitions.clear();
			
			double [] variables = solver.getPtrVariables();
			for (int i = 0; i < variables.length / N; i++)
			{
				ArrayList<Integer> partition = new ArrayList<Integer>();
				
				for (int j = 0; j < N; j++)
				{
					int index = i * N + j;
					
					double value = variables[index];
					
					// There might be an oddball case where the solution is 0.99999999
					// or 1.00000001 because of rounding errors. Therefore, just 
					// treat this case as being equal to 1.
					if (value > 0.9999 && value <= 1.0001)
						partition.add(j);
					else
					{
						// If it's not a one, it might be a fractional value. If
						// this is the case, then indicate it by storing -1.0 into
						// the results array.
						if (value != 0.0 && value / ((int)value) != 1.0)
							results[1] = -1.0;
					}
				}
				
				partitions.add(partition);
			}
		}
		catch (Exception ex)
		{
			System.err.println("Error constructing linear program.");
			ex.printStackTrace();
		}
		finally
		{
			if (solver != null)
				solver.deleteLp();
		}
		
		return results;
	}
	
	
	/**
	 * Helper method which constructs the objective function. The 
	 * objective function is simply the minimum of the summation of
	 * every processor cost multiplied by every variable.
	 * 
	 * @return A String containing the appropriate coefficients to
	 * be input into the LP solver.
	 */
	private String constructObjectiveFunction()
	{
		String result = "";
		
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
				result += _processors[i].getCost() + " ";
		}
		
		// ----------------------------------------------------
		// DEBUG CODE
		// ----------------------------------------------------
		if (DEBUG)
		{
			System.out.println("\n\nOBJECTIVE FUNCTION");
			System.out.println(result);
		}
		// ----------------------------------------------------
		
		return result.trim();
	}
	
	
	/**
	 * Get the constraints for each variable. These constraints just state
	 * that each variable must be greater than or equal to 0.
	 * 
	 * @return A list of constraints.
	 */
	private ArrayList<String> variableConstraints()
	{
		ArrayList<String> results = new ArrayList<String>();
		
		for (int i = 0; i < N * N; i++)
		{
			double [] constraint = new double[N * N];
			constraint[i] = 1;
			
			results.add(constructStrLpConstraint(constraint));
		}
		
		// ----------------------------------------------------
		// DEBUG CODE
		// ----------------------------------------------------
		if (DEBUG)
		{
			System.out.println("\n\nVARIABLE CONSTRAINTS");
			
			for (String s : results)
				System.out.println(s);
		}
		// ----------------------------------------------------
		
		return results;
	}
	
	
	/**
	 * Helper method which constructs all the straight clique
	 * constraints. These constraints are constructed by finding
	 * all maximal cliques in the graph.
	 * 
	 * @return A list of straight clique constraints.
	 */
	private ArrayList<String> straightCliqueConstraints()
	{
		ArrayList<String> results = new ArrayList<String>();
		
		// Find all of the maximal cliques in the graph.
		BronKerboschCliqueFinder<Integer, DefaultEdge> f 
			= new BronKerboschCliqueFinder<Integer, DefaultEdge>(_graph);
			
		Collection<Set<Integer>> cliques = f.getAllMaximalCliques();
					
		// Iterate through all the maximal cliques which were found
		// from the graph and construct the constraints for these.
		Iterator<Set<Integer>> all_cliques = cliques.iterator();
		while (all_cliques.hasNext())
		{
			// Store the clique in a temporary array which will
			// be used to construct the final constraint.
			int [] coeff = new int[N];
			
			// Iterate through the nodes in the clique and store them in
			// the above array.
			Iterator<Integer> clique = all_cliques.next().iterator();
			while (clique.hasNext())
			{
				int node = clique.next();
				coeff[node] = 1;
			}

			// Construct the constraint. Each maximal clique will have
			// N corresponding constraints because of the graph cartesian
			// product. Instead of actually doing the product of two graphs,
			// which is unnecessary, we can just construct the matrix.
			for (int i = 0; i < N; i++)
			{
				double [] constraint = new double[N * N];
				for (int j = 0; j < coeff.length; j++)
					constraint[i * N + j] = coeff[j];
				
				results.add(constructStrLpConstraint(constraint));
			}
		}
		
		// ----------------------------------------------------
		// DEBUG CODE
		// ----------------------------------------------------
		if (DEBUG)
		{
			System.out.println("\n\nSTRAIGHT CLIQUE CONSTRAINTS");
			
			for (String s : results)
				System.out.println(s);
		}
		// ----------------------------------------------------
				
		return results;
	}
	
	
	/**
	 * Constructs the cross clique constraints. These constraints
	 * simply utilize the properties of multiplying a graph by its
	 * own complete graph. By doing so, the cross clique constraints
	 * simply state that a job can only be present in one and only
	 * one processor.
	 * 
	 * @return A list of all the cross clique constraints.
	 */
	private ArrayList<String> crossCliqueConstraints()
	{
		ArrayList<String> results = new ArrayList<String>();
		
		// Constructs the cross clique constraints which simply
		// ensures that no node is found in more than one processor.
		// It can just be constructed without finding the cliques.
		for (int i = 0; i < N; i++)
		{
			double [] constraint = new double[N * N];
			for (int j = 0; j < N; j++)
				constraint[j * N + i] = 1;
			
			results.add(constructStrLpConstraint(constraint));
		}
		
		// ----------------------------------------------------
		// DEBUG CODE
		// ----------------------------------------------------
		if (DEBUG)
		{
			System.out.println("\n\nCROSS CLIQUE CONSTRAINTS");
			
			for (String s : results)
				System.out.println(s);
		}
		// ----------------------------------------------------
				
		return results;
	}
	
	
	/**
	 * Constructs a string representation of a constraint that is 
	 * stored in a double array. For some reason, passing in double
	 * arrays into a LP solver removes the first element. Therefore, I
	 * just convert it to a String instead.
	 * 
	 * @param input_constraint The constraint to convert to a string.
	 * 
	 * @return A string representation of the constraint.
	 */
	private String constructStrLpConstraint(double [] input_constraint)
	{
		String result_constraint = "";
		
		for (double coeff : input_constraint)
			result_constraint += coeff + " ";
		
		return result_constraint.trim();
	}
	
	
	/**
	 * Adds constraints to the solver.
	 * 
	 * @param constraints A list of constraints to add to the solver.
	 * @param solver The LpSolve object to add the constraints to.
	 * @param constraint_type The type of constraint (e.g. >=, =, etc.).
	 * @param rh_value The right hand value of the constraint equation.
	 */
	private void addLpConstraints(
			ArrayList<String> constraints,
			LpSolve solver, 
			int constraint_type, 
			double rh_value)
	{
		try
		{
			// Add each constraint in the list to the solver.
			for (String constraint : constraints)
			{
				solver.strAddConstraint(constraint, constraint_type, rh_value);
			}
		}
		catch (LpSolveException ex)
		{
			System.err.println("Error adding constraint...");
			ex.printStackTrace();
		}
	}
}
